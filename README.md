# Cours Crafts par Wemanity - Travail Pratique 

## Consignes 4 mai 2020 13h30

* Réalisation par équipe de 5-6 personnes. Envoyez moi un email avec la liste des membres de l’équipe.
* Réalisation de l'application demandée qui répond aux besoins exprimés (voir besoins)
* Date limite de livraison : 27 mai 23:59, envoi par email zip du code source (assurez-vous d'inclure seulement le code pas les binaires).
* Réalisation de l'application avec le technologies suivantes :

- Java 8+ ou kotlin 
- Maven ou Gradle
- Docker
- Angular 9+ (Node 12+)

Vous pouvez choisir de développer une application avec seulement Java ou Java + Angular. Vous avez le droit d'utiliser des frameworks ou librairies du marché. Ce cours n'a pas pour but de vous apprendre à développer sur Java ou Angular, mais assume que vous savez déjà le faire. Quoi qu'il en soit, le choix technologique n'est pas important, mais __la qualité du code livré l'est__.

## Besoins

En tant que fondateur d’une startup, j’ai constaté une belle opportunit pour lancer un nouveau produit. Depuis ces cinq dernières années, le marché est très tendu et est sur un point tournant. 
Les sociétés, de petites et grandes structures, ont de plus en plus besoin d'experts IT 
afin de développer les applications qui répondent à leurs besoins en temps et en heure. 

J'aimerais pouvoir héberger un ensemble de profils en ligne, au même titre que monster ou linkedIn. 
J'ai donc besoin d'une application avec les fonctionnalités suivantes :

1. lister un ensemble de profils
2. afficher les compétences d'un profil
    - 5 sections (sommaire, expérience, formation, compétences techniques, études)
3. rechercher les profils par mots clés (un ou plusieurs mots)
    - afficher cette liste par ordre selon pertinence
4. publier mon profil (éditer les différentes sections et écriture en base de données)

Bonus :
5. télécharger mon profil (avec un fichier Word ou via linkedIn) + 100

## Critère d’évaluation :

- La qualité      /250
    - Un readme.md à la racine du project détaillant l’exercice, permettant le hand-over pour une reprise plus facile par une autre équipe /50
    - Lisibilité du code /100
    - Propreté du code / 60
    - Ré-utilisabilité du code /40
-  Le testing. /300
    - Test unitaire /140
    - Test d’intégration /80
    - Test d’acceptance /80
- Architecture  /150
    - Structure des classes méthodes /50
    - Structure des modules /50
    - Pertinence vs les principes SOLID /50
- Integration continue /150
    - Présence d’un pipeline de build et sa pertinence /70
    - Mise en évidence des builds (failles et success) dans l’historique d’évolution du projet /30
    - Utilisation de docker pour déployer l’application front+back /50
- Design patterns  /150
    - Implémentation propre par pattern : +50
- Bonus    / 330 bonus
    - L’application est complète, cad toutes les fonctionnalités sont délivrées /100
    - Rédiger markdown par design pattern expliquant le pattern et comment ça s’articule dans le projet /50
    - La pertinence des fonctionnalités implémentées /30
    - Créativité sur le design UI/UX  /50
    - fonctionalité de téléchargement de cv /100
- Malus
    - chaque bug -50
    - livraison en retard -200, puis -10 par heure, non acceptée après le 28 mai 12h59
    - le code dans le zip ne compile pas -100
    - l'application compilée ne tourne pas -100

## Note du TP

Sous-total : /1000
Bonus : /330
Malus : /450

La note individuelle = 50% du total + (50% du total x note-appéciation-des-autres/5). 
Je ne vous communiquerai pas le détail des notes individuelles ou par équipe.

Le total sera ramené à /75, car compté 75% de de la note finale.

Vous n’obtiendrez pas les 75 points. Optimisez vos efforts pour obtenir le maximum de points.

Bon courage!
